# DBE Awards

A website for a group of friends to host their own awards show.

This website will:

- Allow users to log in and add awards to the 'awards show', and also allocate their winners for all of the awards
- Allow any user to 'host' the awards show, which will go through and present the awards in sequence

## For Developers
### Installing dependencies
(Commands were run on an Ubuntu machine, Windows/MacOS may vary)

- Make sure python3 is installed: `python3 -V`
  - Make sure pip is installed too: `pip -V`
- Set up a venv for this project: `python3 -m venv django-environment`
  - Activate the venv `source django-environment/bin/activate`
  - If need to deactivate the venv at any point, use `deactivate`
- Set up local mysql server:
  - `sudo apt install mysql-server`
  - `sudo systemctl start mysql.service`
  - `sudo mysql`
  - `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '[some password]';`
  - `sudo mysql_secure_installation`
    - accept defaults for the most part
  - `sudo mysql -uroot -p`
  - `ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;` to restore default
  - `CREATE USER '[some username]'@'localhost' IDENTIFIED BY '[some password]';`
  - `GRANT CREATE, ALTER, DROP, INSERT, UPDATE, INDEX, DELETE, SELECT, REFERENCES, RELOAD on *.* TO '[some username]'@'localhost' WITH GRANT OPTION;`
  - `FLUSH PRIVILEGES;`
  - Put details into .env file in `/dbe-awards/dbesite`. The `.env.example` file can be used as a template.
  - `pip install python-dotenv`
- Install [mysqlclient](https://pypi.org/project/mysqlclient/)
  - `sudo apt-get install python3-dev default-libmysqlclient-dev build-essential`
  - `pip install mysqlclient`
- Install django: `python3 -m pip install Django`
  - `python manage.py migrate`
  - `python manage.py createsuperuser`

### Misc commands:
- Run development server: `python manage.py runserver [optional ip:][optional port number]`
- Create a new app: `python manage.py startapp [app name]`
- `python manage.py changepassword *username*`
