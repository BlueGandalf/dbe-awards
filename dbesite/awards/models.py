from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class AwardShow(models.Model):
    award_show_title = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.award_show_title

    def save(self, *args, **kwargs):
        if self.is_active:
            if not self.pk:  # object is being created, thus no primary key field yet
                AwardShow.objects.all().update(is_active=False)
            else:
                AwardShow.objects.all().exclude(pk=self.pk).update(is_active=False)

        super(AwardShow, self).save(*args, **kwargs)

class Award(models.Model):
    award_name = models.CharField(max_length=50)
    award_show = models.ForeignKey(AwardShow, on_delete=models.CASCADE, null=True)
    index = models.IntegerField(default=0)

    def __str__(self):
        return self.award_name

    def save(self, *args, **kwargs):
        if not self.pk:  # object is being created, thus no primary key field yet
            if self.award_show is None:
                self.award_show = AwardShow.objects.get(is_active=True)
            if not self.index:
                self.index = Award.objects.filter(award_show=self.award_show).count() + 1

        super(Award, self).save(*args, **kwargs)

        other_awards_at_same_index = Award.objects.filter(award_show=self.award_show, index=self.index)
        if other_awards_at_same_index.count() > 1:
            for other_award in other_awards_at_same_index.exclude(pk=self.pk):
                for x in range(1, Award.objects.filter(award_show=self.award_show).count() + 1):
                    if Award.objects.filter(award_show=self.award_show, index=x).count() == 0:
                        other_award.index = x
                        other_award.save()
                        break

class Winner(models.Model):
    award = models.ForeignKey(Award, on_delete=models.CASCADE)
    voter = models.ForeignKey(User, on_delete=models.CASCADE)
    winner_text = models.CharField(max_length=200)
    link = models.CharField(max_length=200, default='#')

    def __str__(self):
        return self.winner_text

    def save(self, *args, **kwargs):
        super(Winner, self).save(*args, **kwargs)
        if Winner.objects.filter(award=self.award, voter=self.voter).count() > 1:
            Winner.objects.filter(award=self.award, voter=self.voter).exclude(pk=self.pk).delete()
