from django.urls import path

from . import views

app_name = 'awards'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('award/<int:pk>', views.AwardDetailView.as_view(), name='award_detail'),
    path('award/<int:award_id>/vote', views.vote, name='award_vote'),
    path('award/add', views.addAward, name='award_add'),
    path('api/awardshow', views.AwardShowViewSet.as_view(), name='award_api_awardshow'),
]