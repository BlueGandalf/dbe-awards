from django.test import TestCase

from django.contrib.auth.models import User
from .models import AwardShow, Award, Winner

# Create your tests here.
class AwardShowTests(TestCase):
    def test_new_award_show_is_set_as_active_by_default(self):
        """
        a new AwardShow will have 'is_active' set to True by default
        """
        new_award_show = AwardShow(award_show_title="test")
        new_award_show.save()
        self.assertIs(new_award_show.is_active, True)

    def test_new_active_award_show_is_only_active_award_show(self):
        """
        if a new AwardShow is 'active', set all other AwardShows to inactive
        """
        oldest_award_show = AwardShow(award_show_title="oldest", is_active=True)
        oldest_award_show.save()
        older_award_show = AwardShow(award_show_title="older", is_active=True)
        older_award_show.save()
        new_award_show = AwardShow(award_show_title="new", is_active=True)
        new_award_show.save()

        self.assertEqual(AwardShow.objects.filter(is_active=True).count(), 1)
        self.assertEqual(AwardShow.objects.get(is_active=True), new_award_show)
        oldest_award_show.refresh_from_db()
        older_award_show.refresh_from_db()
        new_award_show.refresh_from_db()
        self.assertIs(oldest_award_show.is_active, False)
        self.assertIs(older_award_show.is_active, False)
        self.assertIs(new_award_show.is_active, True)

    def test_existing_award_show_set_to_active_is_only_active_award_show(self):
        """
        if an existing AwardShow is 'active', set all other AwardShows to inactive
        """
        oldest_award_show = AwardShow(award_show_title="oldest", is_active=True)
        oldest_award_show.save()
        older_award_show = AwardShow(award_show_title="older", is_active=True)
        older_award_show.save()
        new_award_show = AwardShow(award_show_title="new", is_active=True)
        new_award_show.save()
        oldest_award_show.is_active = True
        oldest_award_show.save()

        self.assertEqual(AwardShow.objects.filter(is_active=True).count(), 1)
        self.assertEqual(AwardShow.objects.get(is_active=True), oldest_award_show)
        oldest_award_show.refresh_from_db()
        older_award_show.refresh_from_db()
        new_award_show.refresh_from_db()
        self.assertIs(oldest_award_show.is_active, True)
        self.assertIs(older_award_show.is_active, False)
        self.assertIs(new_award_show.is_active, False)

class AwardTests(TestCase):
    def test_default_award_show_is_the_active_award_show(self):
        """
        when a new award is added, by default assign the award_show to the currently active one
        """
        active_award_show = AwardShow(award_show_title="active", is_active=True)
        active_award_show.save()
        inactive_award_show = AwardShow(award_show_title="inactive", is_active=False)
        inactive_award_show.save()

        new_award = Award(award_name="new")
        new_award.save()

        self.assertEqual(new_award.award_show, active_award_show)

    def test_default_index_is_the_last_in_the_award_show(self):
        """
        when a new award is added, by default assign the index to be 1 higher than the number of other awards in that award_show;
        put it at the end of the award_show's queue
        """
        award_show = AwardShow(award_show_title="test", is_active=True)
        award_show.save()
        oldest_award = Award(award_name="oldest")
        oldest_award.save()
        older_award = Award(award_name="older")
        older_award.save()
        new_award = Award(award_name="new")
        new_award.save()

        self.assertEqual(oldest_award.index, 1)
        self.assertEqual(older_award.index, 2)
        self.assertEqual(new_award.index, 3)

    def test_if_duplicate_indexes_swap_index(self):
        """
        When an award is saved, if the index is the same as another for the same award show, then swap the other awards to open indexes
        """
        award_show = AwardShow(award_show_title="test", is_active=True)
        award_show.save()
        award_one = Award(award_name="oldest")#default index 1
        award_one.save()
        award_two = Award(award_name="older")#default index 2
        award_two.save()
        award_three = Award(award_name="old")#default index 3
        award_three.save()
        award_four = Award(award_name="new", index=2)#index 2
        award_four.save()# award_two.index should be changed to 4

        award_three.index = 1
        award_three.save()# award_one.index should be changed to 3

        award_one.refresh_from_db()
        award_two.refresh_from_db()
        award_three.refresh_from_db()
        award_four.refresh_from_db()

        self.assertEqual(award_one.index, 3)
        self.assertEqual(award_two.index, 4)
        self.assertEqual(award_three.index, 1)
        self.assertEqual(award_four.index, 2)

class WinnerTests(TestCase):
    def test_delete_duplicate_winners(self):
        """
        Each user should only have one winner per award
        If a new winner is added, the old winner should be deleted
        """
        user = User(username="test")
        user.save()
        award_show = AwardShow(award_show_title="test")
        award_show.save()
        award = Award(award_name="test award")
        award.save()
        winner1 = Winner(winner_text="winner 1", voter=user, award=award)
        winner1.save()
        winner2 = Winner(winner_text="winner 2", voter=user, award=award)
        winner2.save()

        self.assertEqual(Winner.objects.filter(voter=user, award=award).count(), 1)
        self.assertEqual(Winner.objects.get(voter=user, award=award), winner2)