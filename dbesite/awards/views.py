from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from rest_framework import generics

from .models import Award, AwardShow, Winner
from .serializers import AwardSerializer, AwardShowSerializer, WinnerSerializer

class AwardShowViewSet(generics.ListAPIView):
    """
    API endpoint that allows all awards in the active awardshow to be viewed.
    """
    queryset = AwardShow.objects.all()
    serializer_class = AwardShowSerializer

class IndexView(generic.ListView):
    template_name = 'awards/index.html'
    context_object_name = 'awards_list'

    def get_queryset(self):
        """Returns all awards for the active award show."""
        active_award_show = get_object_or_404(AwardShow, is_active=True) 
        return Award.objects.filter(award_show=active_award_show).order_by('index')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['user_voted_awards'] = context['awards_list'].filter(winner__voter=self.request.user)

        return context

class AwardDetailView(generic.DetailView):
    model = Award
    template_name = 'awards/detail.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        current_user = self.request.user
        try:
            context['current_winner'] = context['award'].winner_set.get(voter=current_user)
        except Winner.DoesNotExist:
            context['current_winner'] = None

        return context

def vote(request, award_id):
    if (not request.user.is_authenticated):
        return HttpResponseRedirect(reverse('login'))

    award = get_object_or_404(Award, pk=award_id)
    if (request.POST['winner_name'] == ''):
        return render(request, 'awards:award_detail', {
            'award': award,
            'error_message': "You didn't enter a winner.",
        })

    new_winner = Winner(winner_text=request.POST['winner_name'], award=award, voter=request.user, link=request.POST['winner_link'])
    new_winner.save()

    return HttpResponseRedirect(reverse('awards:index'))

def addAward(request):
    if (not request.user.is_authenticated):
        return HttpResponseRedirect(reverse('login'))

    if (request.POST['award_name'] == ''):
        return render(request, 'awards:index', {
            'error_message': "You didn't enter a title.",
        })
    
    new_award = Award(award_name=request.POST['award_name'])
    new_award.save()

    return HttpResponseRedirect(reverse('awards:index'))
