from django.contrib import admin

from .models import Award
from .models import AwardShow

# Register your models here.
admin.site.register(Award)
admin.site.register(AwardShow)
