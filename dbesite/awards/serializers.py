from rest_framework import serializers

from .models import Award, AwardShow, Winner

class WinnerSerializer(serializers.ModelSerializer):
    voter = serializers.StringRelatedField()

    class Meta:
        model = Winner
        fields = ['voter', 'winner_text', 'link']

class AwardSerializer(serializers.ModelSerializer):
    winner_set = WinnerSerializer(many=True, read_only=True)

    class Meta:
        model = Award
        fields = ['award_name', 'index', 'winner_set']

class AwardShowSerializer(serializers.ModelSerializer):
    award_set = AwardSerializer(many=True, read_only=True)

    class Meta:
        model = AwardShow
        fields = ['award_show_title', 'award_set']
