function Winner(props) {
  return (
    <div className="container">
        <div className="accordion-item">
            <h2 className="accordion-header" id={"flush-heading-" + props.uniqueId}>
            <button className="accordion-button collapsed text-center" type="button" data-bs-toggle="collapse" data-bs-target={"#flush-collapse-" + props.uniqueId} aria-expanded="false" aria-controls="flush-collapseOne">
                <div className="lead fs-2">
                    {props.voter}
                </div>
            </button>
            </h2>
            <div id={"flush-collapse-" + props.uniqueId} className="accordion-collapse collapse" aria-labelledby={"flush-heading-" + props.uniqueId} data-bs-parent={"accordion-" + props.award_name}>
                <div className="accordion-body d-flex h-100 align-items-center justify-content-center">
                    <span className="lead fs-1 text-center"><a href={props.link} target="_blank" rel="noreferrer">{props.text}</a></span>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Winner;
