import '../style/Award.css'
import Winner from './Winner'

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex !== 0) {
  
        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
    
        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }
  
    return array;
}

function Award(props) {

    const winners = [];
    props.winners.forEach(winner => {
        winners.push(<Winner key={"winner-" + props.index + "-" + winner.voter} uniqueId={"winner-" + props.index + "-" + winner.voter} award_name={props.name} voter={winner.voter} text={winner.winner_text} link={winner.link}></Winner>)
    });

  return (
    <div className={"carousel-item .bg-secondary.bg-gradient" + (props.active ? " active" : "")}>
        <div className="d-flex h-100 align-items-center justify-content-center">
            <span className="award-title h1">{props.name}</span>
        </div>
        <div className="container d-flex h-100 align-items-center justify-content-center">
            <div className="accordion accordion-flush" id={"accordion-" + props.name}>
                {shuffle(winners)}
            </div>
        </div>
    </div>
  );
}

export default Award;
