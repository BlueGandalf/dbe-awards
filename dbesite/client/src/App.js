import axios from "axios";
import React from "react";

import Award from "./components/Award"

import './App.css';

function App() {
  const [awardShow, setAwardShow] = React.useState(null);

  React.useEffect(() => {
    axios.get(process.env.REACT_APP_API_BASE_URL, {
      auth: {
        username: process.env.REACT_APP_API_USERNAME,
        password: process.env.REACT_APP_API_PASSWORD,
      },
    }).then((response) => {
      setAwardShow(response.data[0]);
    });
  }, []);

  if (!awardShow) return null;
  
  const awards = [];
  let active = true;
  awardShow.award_set.forEach(award => {
    awards.push(<Award key={"award-" + award.index} name={award.award_name} winners={award.winner_set} index={award.index} active={active}></Award>)
    
    if (active) active = false;
  });

  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg bg-body-tertiary bg-dark" data-bs-theme="dark">
        <div className="container-fluid">
          <span className="navbar-brand">{awardShow.award_show_title}</span>
        </div>
      </nav>
      <div className="container">
        <div id="awardShowCarousel" className="carousel slide" data-bs-theme="dark">
          <div className="carousel-inner">
            {awards}
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#awardShowCarousel" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#awardShowCarousel" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
